# SiliCompressor

本项目是基于开源项目**Tourenathan-G5organisation/SiliCompressor**进行适用harmonyos的移植，可以通过项目标签以及 github地址https://github.com/Tourenathan-G5organisation/SiliCompressor 追踪到原项目

移植版本：v2.2.4

## 1. 项目介绍
### 项目名称：SiliCompressor
### 所属系列：harmonyos的第三方组件适配移植
### 实现功能：
1. 图片压缩功能。

### 未实现功能：
1. 视频压缩功能。

### 调用差异：暂无
### 联系方式：[https://gitee.com/ts_ohos/silicompressor-for-ohos/issues](https://gitee.com/ts_ohos/silicompressor-for-ohos/issues)
### 原项目Doc地址：[https://github.com/Tourenathan-G5organisation/SiliCompressor](https://github.com/Tourenathan-G5organisation/SiliCompressor)
### 编程语言：java

## 2. 集成指引
### 方式一：
1. 下载或自行编译生成silicompressor-for-ohos的.har文件，文件路径为：./entry/libs/silicompressor-debug.har。
2. 自行编译时，需要注意要自行添加签名。
3. 导入你的harmonyos项目模块的**./libs**中。
4. 在模块下的**build.gradle**中确认依赖**./libs**下的.har包，``implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])``。
5. 在代码中使用。

### 方式二：
1. 在根目录的`build.gradle`文件中添加`mavenCentral()`：
   ```groovy
   // Top-level build file where you can add configuration options common to all sub-projects/modules.
   ...
   buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
   ```

2. 在module目录下的`build.gradle`文件中添加``

   ```groovy
   ...
   dependencies {
       ...
       implementation 'com.gitee.ts_ohos:silicompressor-for-ohos:1.0.1'
   }
   ```

## 3. 使用说明
参考本项目的**Java Doc**与**OHOS**的文档。

Java doc : [https://github.com/Tourenathan-G5organisation/SiliCompressor](https://github.com/Tourenathan-G5organisation/SiliCompressor)

```json
/** Related permissions. */
{
  "other_option": "other_option",
  "module": {
    "other_option": "other_option",
    "reqPermissions": [
      {
        "name": "ohos.permission.READ_USER_STORAGE"
      },
      {
        "name": "ohos.permission.WRITE_USER_STORAGE"
      }
    ]
  }
}
```

```java
/** 压缩图片文件，输出图片文件。 */
String outputFile = SiliCompressor.with(getContext())
        .compress(/** Original image uri. */ obtainImgUri().toString(),
                  /** Output image file path. */ new File(BUNDLE_SANDBOX_PREFIX));

/** 压缩图片文件，输出图片文件，选择是否删除源图片文件。 */
String outputFile = SiliCompressor.with(getContext())
        .compress(/** Original image uri. */ obtainImgUri().toString(),
                  /** Output image file path. */ new File(BUNDLE_SANDBOX_PREFIX),
                  /** If you need to delete the original image file. */ false);
```

```java
/** 压缩Element图片资源，输出图片文件。 */
String outputFile = SiliCompressor.with(getContext())
        .compress(/** Original Element resource */ ResourceTable.Media_test_compress_element,
                  /** Output image file path. */ BUNDLE_SANDBOX_PREFIX);
```

```java
/** 压缩图片文件，输出PixelMap。 */
PixelMap outputPixelMap = SiliCompressor.with(getContext())
        .getCompressBitmap(/** Original image uri. */ obtainImgUri().toString(),
                           /** Temp dir for image compressing. */ BUNDLE_SANDBOX_PREFIX);

/** 压缩图片文件，输出PixelMap，选择是否删除源图片文件。 */
PixelMap outputPixelMap = SiliCompressor.with(getContext())
        .getCompressBitmap(/** Original image uri. */ obtainImgUri().toString(),
                           /** If you need to delete the original image file. */ needRemoveSource,
                           /** Temp dir for image compressing. */ BUNDLE_SANDBOX_PREFIX);
```

## 4. 效果演示
1. 压缩后图片和图片信息
<img src="./pic/demo_1.jpg" width="200">

## 5. 版本迭代
- v1.0.0 基于原项目最新版本，初次提交。
- v1.0.1 升级harmonyos API为5。

## 6. 版本和许可信息

- Apache License 2.0
- [https://gitee.com/ts_ohos/silicompressor-for-ohos/blob/ohos_main/LICENSE](https://gitee.com/ts_ohos/silicompressor-for-ohos/blob/ohos_main/LICENSE)
```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


package com.iceteck.silicompressorrforohos.videocompression;

import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.graphics.TextureHolder;
import ohos.agp.render.opengl.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class OutputSurface implements TextureHolder.OnNewFrameCallback{
    private static final int EGL_OPENGL_ES2_BIT = 4;
    private static final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
    private EGL mEGL;
    private EGLDisplay mEGLDisplay = null;
    private EGLContext mEGLContext = null;
    private EGLSurface mEGLSurface = null;
    private TextureHolder textureHolder;
    SurfaceOps ops;
    private Surface mSurface;
    private final Object mFrameSyncObject = new Object();
    private boolean isMmFrameAvailable;
    private TextureRenderer mTextureRender;
    private int mWidth;
    private int mHeight;
    private int rotateRender = 0;
    private ByteBuffer mPixelBuf;

    public OutputSurface(int width, int height, int rotate) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException();
        }
        mWidth = width;
        mHeight = height;
        rotateRender = rotate;
        mPixelBuf = ByteBuffer.allocateDirect(mWidth * mHeight * 4);
        mPixelBuf.order(ByteOrder.LITTLE_ENDIAN);
        eglSetup(width, height);
        makeCurrent();
        setup();
    }

    public OutputSurface() {
        setup();
    }

    private void setup() {
        mTextureRender = new TextureRenderer(rotateRender);
        mTextureRender.surfaceCreated();
        textureHolder = new TextureHolder(mTextureRender.getTextureId());

        textureHolder.setOnNewFrameCallback(this);
        mSurface = new Surface();
        mSurface.bindToTextureHolder(textureHolder);
    }

    private void eglSetup(int width, int height) {
        mEGL = new EGL();
        mEGLDisplay = mEGL.eglGetDisplay(EGL.EGL_DEFAULT_DISPLAY);

        if (mEGLDisplay == EGL.EGL_NO_DISPLAY) {
            throw new RuntimeException("unable to get EGL10 display");
        }

        if (!mEGL.eglInitialize(mEGLDisplay, null,null)) {
            mEGLDisplay = null;
            throw new RuntimeException("unable to initialize EGL10");
        }

        int[] attribList = {
                EGL.EGL_RED_SIZE, 8,
                EGL.EGL_GREEN_SIZE, 8,
                EGL.EGL_BLUE_SIZE, 8,
                EGL.EGL_ALPHA_SIZE, 8,
                EGL.EGL_SURFACE_TYPE, EGL.EGL_PBUFFER_BIT,
                EGL.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                EGL.EGL_NONE
        };
        EGLConfig[] configs = new EGLConfig[1];
        int[] numConfigs = new int[1];
        if (!mEGL.eglChooseConfig(mEGLDisplay, attribList, configs, configs.length, numConfigs)) {
            throw new RuntimeException("unable to find RGB888+pbuffer EGL config");
        }
        int[] attrib_list = {
                EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL.EGL_NONE
        };
        mEGLContext = mEGL.eglCreateContext(mEGLDisplay, configs[0], EGL.EGL_NO_CONTEXT, attrib_list);
        checkEglError("eglCreateContext");
        if (mEGLContext == null) {
            throw new RuntimeException("null context");
        }
        int[] surfaceAttribs = {
                EGL.EGL_WIDTH, width,
                EGL.EGL_HEIGHT, height,
                EGL.EGL_NONE
        };
        mEGLSurface = mEGL.eglCreatePbufferSurface(mEGLDisplay, configs[0], surfaceAttribs);
        checkEglError("eglCreatePbufferSurface");
        if (mEGLSurface == null) {
            throw new RuntimeException("surface was null");
        }
    }

    public void release() {
        if (mEGL != null) {
            if (mEGL.eglGetCurrentContext().equals(mEGLContext)) {
                mEGL.eglMakeCurrent(mEGLDisplay, EGL.EGL_NO_SURFACE, EGL.EGL_NO_SURFACE, EGL.EGL_NO_CONTEXT);
            }
            mEGL.eglDestroySurface(mEGLDisplay, mEGLSurface);
            mEGL.eglDestroyContext(mEGLDisplay, mEGLContext);
        }
        mSurface.syncCanvasDrawCalls();
        mEGLDisplay = null;
        mEGLContext = null;
        mEGLSurface = null;
        mEGL = null;
        mTextureRender = null;
        mSurface = null;
        textureHolder = null;
    }

    public void makeCurrent() {
        if (mEGL == null) {
            throw new RuntimeException("not configured for makeCurrent");
        }
        checkEglError("before makeCurrent");
        if (!mEGL.eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext)) {
            throw new RuntimeException("eglMakeCurrent failed");
        }
    }

    public Surface getSurface() {
        return mSurface;
    }

    public void changeFragmentShader(String fragmentShader) {
        mTextureRender.changeFragmentShader(fragmentShader);
    }

    public void awaitNewImage() {
        final int TIMEOUT_MS = 5000;
        synchronized (mFrameSyncObject) {
            while (!isMmFrameAvailable) {
                try {
                    mFrameSyncObject.wait(TIMEOUT_MS);
                    if (!isMmFrameAvailable) {
                        throw new RuntimeException("Surface frame wait timed out");
                    }
                } catch (InterruptedException ie) {
                    throw new RuntimeException(ie);
                }
            }
            isMmFrameAvailable = false;
        }
        mTextureRender.checkGlError("before updateTexImage");
        textureHolder.refreshTextureImage();
    }

    public void drawImage(boolean isInvert) {
        mTextureRender.drawFrame(textureHolder, isInvert);
    }

    @Override
    public void onNewFrame(TextureHolder textureHolder) {
        synchronized (mFrameSyncObject) {
            if (isMmFrameAvailable) {
                throw new RuntimeException("mFrameAvailable already set, frame could be dropped");
            }
            isMmFrameAvailable = true;
            mFrameSyncObject.notifyAll();
        }
    }

    public ByteBuffer getFrame() {
        mPixelBuf.rewind();
        GLES20.glReadPixels(0, 0, mWidth, mHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixelBuf);
        return mPixelBuf;
    }

    private void checkEglError(String msg) {
        if (mEGL.eglGetError() != EGL.EGL_SUCCESS) {
            throw new RuntimeException("EGL error encountered (see log)");
        }
    }
}

package com.iceteck.silicompressorrforohos.videocompression;

import ohos.agp.graphics.Surface;
import ohos.agp.render.opengl.*;

public class InputSurface {
    private static final int EGL_RECORDABLE_ANDROID = 0x3142;
    private static final int EGL_OPENGL_ES2_BIT = 4;
    private EGLDisplay mEGLDisplay;
    private EGLContext mEGLContext;
    private EGLSurface mEGLSurface;
    private Surface mSurface;

    public InputSurface(Surface surface) {
        if (surface == null) {
            throw new NullPointerException();
        }
        mSurface = surface;
        eglSetup();
    }

    private void eglSetup() {
        mEGLDisplay = EGL.eglGetDisplay(EGL.EGL_DEFAULT_DISPLAY);
        if (mEGLDisplay == EGL.EGL_NO_DISPLAY) {
            throw new RuntimeException("unable to get EGL14 display");
        }
        int[] version = new int[2];
        if (!EGL.eglInitialize(mEGLDisplay, version, version)) {
            mEGLDisplay = null;
            throw new RuntimeException("unable to initialize EGL14");
        }

        int[] attribList = {
                EGL.EGL_RED_SIZE, 8,
                EGL.EGL_GREEN_SIZE, 8,
                EGL.EGL_BLUE_SIZE, 8,
                EGL.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                EGL_RECORDABLE_ANDROID, 1,
                EGL.EGL_NONE
        };
        EGLConfig[] configs = new EGLConfig[1];
        int[] numConfigs = new int[1];
        if (!EGL.eglChooseConfig(mEGLDisplay, attribList, configs, configs.length, numConfigs)) {
            throw new RuntimeException("unable to find RGB888+recordable ES2 EGL config");
        }

        int[] attrib_list = {
                12440, 2,
                EGL.EGL_NONE
        };

        mEGLContext = EGL.eglCreateContext(mEGLDisplay, configs[0], EGL.EGL_NO_CONTEXT, attrib_list);
        checkEglError("eglCreateContext");
        if (mEGLContext == null) {
            throw new RuntimeException("null context");
        }

        int[] surfaceAttribs = {
                EGL.EGL_NONE
        };
        mEGLSurface = EGL.eglCreateWindowSurface(mEGLDisplay, configs[0], mSurface, surfaceAttribs);
        checkEglError("eglCreateWindowSurface");
        if (mEGLSurface == null) {
            throw new RuntimeException("surface was null");
        }
    }

    public void release() {
        if (EGL.eglGetCurrentContext().equals(mEGLContext)) {
            EGL.eglMakeCurrent(mEGLDisplay, EGL.EGL_NO_SURFACE, EGL.EGL_NO_SURFACE, EGL.EGL_NO_CONTEXT);
        }
        EGL.eglDestroySurface(mEGLDisplay, mEGLSurface);
        EGL.eglDestroyContext(mEGLDisplay, mEGLContext);
        mSurface.syncCanvasDrawCalls();
        mEGLDisplay = null;
        mEGLContext = null;
        mEGLSurface = null;
        mSurface = null;
    }

    public void makeCurrent() {
        if (!EGL.eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext)) {
            throw new RuntimeException("eglMakeCurrent failed");
        }
    }

    public boolean swapBuffers() {
        return EGL.eglSwapBuffers(mEGLDisplay, mEGLSurface);
    }

    public Surface getSurface() {
        return mSurface;
    }

    public void setPresentationTime(long nsecs) {
    }

    private void checkEglError(String msg) {
        boolean isFailed = false;
        int error;
        while ((error = EGL.eglGetError()) != EGL.EGL_SUCCESS) {
            isFailed = true;
        }
        if (isFailed) {
            throw new RuntimeException("EGL error encountered (see log)");
        }
    }
}

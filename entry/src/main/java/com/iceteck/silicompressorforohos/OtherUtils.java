package com.iceteck.silicompressorforohos;

import ohos.app.Context;
import ohos.global.resource.RawFileDescriptor;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class OtherUtils {
    public static final String BUNDLE_SANDBOX_PREFIX = "/data/data/com.iceteck.silicompressorforohos/";
    public static final String BUNDLE_RAWFILE_PATH = "resources/rawfile/";

    public static int getBitRate(long fileSize, int videoTime) {
        int bitRate = 0;
        bitRate = (int) (fileSize * 8 / videoTime);
        return bitRate;
    }

    public static void fakeFile(Context context, String fileName) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry(BUNDLE_RAWFILE_PATH + fileName);
                    Resource resource = rawFileEntry.openRawFile();
                    RawFileDescriptor rawFileDescriptor = rawFileEntry.openRawFileDescriptor();
                    rawFileDescriptor.getFileDescriptor();
                    String newPath = BUNDLE_SANDBOX_PREFIX + fileName;
                    copyFile(resource, newPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).run();
    }

    public static boolean copyFile(InputStream fileInputStream, String newPath) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(newPath);
            byte[] buffer = new byte[1024];
            int byteRead;
            while ((byteRead = fileInputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, byteRead);
            }
            fileInputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
            File file = new File(newPath);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

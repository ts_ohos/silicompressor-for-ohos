/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iceteck.silicompressorforohos.slice;

import com.iceteck.silicompressorforohos.ResourceTable;
import com.iceteck.silicompressorrforohos.SiliCompressor;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.File;

public class CompressImageFileWithOptionSlice extends BaseCompressSlice {
    private static final String TEST_IMAGE_FILE_PATH = BUNDLE_SANDBOX_PREFIX + "/test_compress_image_file_option.jpeg";

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        mTitle.setText(ResourceTable.String_compress_image_file_with_option);
        mImageInput.setPixelMap(ResourceTable.Media_test_compress_image_file_option);

        File inputFile = new File(TEST_IMAGE_FILE_PATH);
        ImageSource imageSource = ImageSource.create(inputFile, new ImageSource.SourceOptions());
        PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        mTextInput.setText(obtainImageFileInfo(inputFile, pixelMap));

        mBtnCompress.setClickedListener(component -> compressImageFile(TYPE_IMAGE, false));
    }

    private void compressImageFile(int mediaType, boolean needRemoveSource) {
        if (mediaType == TYPE_IMAGE) {
            mSchduleTaskDispatcher.asyncDispatch(() -> {
                String outputFile = SiliCompressor.with(getContext())
                        .compress(obtainImgUri().toString(),
                                new File(BUNDLE_SANDBOX_PREFIX),
                                needRemoveSource);
                mCompressHandler.sendEvent(
                        InnerEvent.get(EVENT_COMPRESS_IMAGE_FILE,
                                TYPE_IMAGE,
                                outputFile));
            });
        }
    }

    private Uri obtainImgUri() {
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        srcOpts.formatHint = "image/jpeg";
        File mFile = new File(TEST_IMAGE_FILE_PATH);
        return Uri.getUriFromFile(mFile);
    }
}

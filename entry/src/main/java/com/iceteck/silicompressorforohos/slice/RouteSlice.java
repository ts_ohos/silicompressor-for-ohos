/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iceteck.silicompressorforohos.slice;

import com.iceteck.silicompressorforohos.OtherUtils;
import com.iceteck.silicompressorforohos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.security.SystemPermission;

public class RouteSlice extends AbilitySlice {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x101, RouteSlice.class.getSimpleName());

    private boolean mPermissionGranted = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_route);
        checkPermissionNeeded();
        findComponentById(ResourceTable.Id_btn_compress_image_file)
                .setClickedListener(component -> {
                    checkPermissionNeeded();
                    if (mPermissionGranted) {
                        present(new CompressImageFileSlice(), new Intent());
                    } else {
                        showErrorInfoAndReqPermission();
                    }
                });

        findComponentById(ResourceTable.Id_btn_compress_image_file_with_option)
                .setClickedListener(component -> {
                    checkPermissionNeeded();
                    if (mPermissionGranted) {
                        present(new CompressImageFileWithOptionSlice(), new Intent());
                    } else {
                        showErrorInfoAndReqPermission();
                    }
                });

        findComponentById(ResourceTable.Id_btn_compress_element)
                .setClickedListener(component -> {
                    checkPermissionNeeded();
                    if (mPermissionGranted) {
                        present(new CompressElementResSlice(), new Intent());
                    } else {
                        showErrorInfoAndReqPermission();
                    }
                });

        findComponentById(ResourceTable.Id_btn_compress_to_pixelmap)
                .setClickedListener(component -> {
                    checkPermissionNeeded();
                    if (mPermissionGranted) {
                        present(new CompressToPixelMapSlice(), new Intent());
                    } else {
                        showErrorInfoAndReqPermission();
                    }
                });

        findComponentById(ResourceTable.Id_btn_compress_to_pixelmap_with_option)
                .setClickedListener(component -> {
                    checkPermissionNeeded();
                    if (mPermissionGranted) {
                        present(new CompressToPixelMapWithOptionSlice(), new Intent());
                    } else {
                        showErrorInfoAndReqPermission();
                    }
                });
    }

    private void showErrorInfoAndReqPermission() {
        new ToastDialog(this).setText(getString(ResourceTable.String_permission_req_info)).show();
        checkPermissionNeeded();
    }

    private void checkPermissionNeeded() {
        if (getAbility().verifySelfPermission(SystemPermission.WRITE_USER_STORAGE) != IBundleManager.PERMISSION_GRANTED
                || getAbility().verifySelfPermission(SystemPermission.WRITE_MEDIA) != IBundleManager.PERMISSION_GRANTED) {
            mPermissionGranted = false;
            getAbility().requestPermissionsFromUser(
                    new String[]{
                            SystemPermission.READ_USER_STORAGE,
                            SystemPermission.WRITE_USER_STORAGE,
                            SystemPermission.READ_MEDIA,
                            SystemPermission.WRITE_MEDIA},
                    0x101);

        } else {
            mPermissionGranted = true;
            OtherUtils.fakeFile(this, "test_compress_image_file.jpeg");
            OtherUtils.fakeFile(this, "test_compress_image_file_option.jpeg");
            OtherUtils.fakeFile(this, "test_compress_element.jpeg");
            OtherUtils.fakeFile(this, "test_compress_image_pixelmap.jpeg");
            OtherUtils.fakeFile(this, "test_compress_image_pixelmap_option.jpeg");
        }
    }

}

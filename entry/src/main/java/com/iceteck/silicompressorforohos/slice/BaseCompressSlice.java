/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iceteck.silicompressorforohos.slice;

import com.iceteck.silicompressorforohos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;
import java.util.Locale;

public class BaseCompressSlice extends AbilitySlice {
    protected static final int TYPE_IMAGE = 1;
    protected static final int TYPE_VIDEO = 2;

    protected static final int EVENT_COMPRESS_IMAGE_FILE = 0x1;
    protected static final int EVENT_COMPRESS_PIXELMAP = 0x2;

    protected static final String BUNDLE_SANDBOX_PREFIX = "/data/data/com.iceteck.silicompressorforohos";

    protected Text mTitle;
    protected Image mImageInput;
    protected Image mImageOutput;
    protected Button mBtnCompress;
    protected Text mTextInput;
    protected Text mTextOutput;

    protected TaskDispatcher mSchduleTaskDispatcher;
    protected CompressHandler mCompressHandler;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_compress_image);

        mTitle = (Text) findComponentById(ResourceTable.Id_compress_title);
        mImageInput = (Image) findComponentById(ResourceTable.Id_img_input);
        mImageOutput = (Image) findComponentById(ResourceTable.Id_img_output);
        mBtnCompress = (Button) findComponentById(ResourceTable.Id_btn_do_compress);
        mTextInput = (Text) findComponentById(ResourceTable.Id_text_input_description);
        mTextOutput = (Text) findComponentById(ResourceTable.Id_text_output_description);

        mSchduleTaskDispatcher = createParallelTaskDispatcher("compress", TaskPriority.DEFAULT);
        mCompressHandler = new CompressHandler(EventRunner.getMainEventRunner());

        mTitle.setText("");
        mTextInput.setText(ResourceTable.String_input_image_info);
        mTextOutput.setText(ResourceTable.String_output_image_info);
        mImageInput.setPixelMap(ResourceTable.Media_def_image);
        mImageOutput.setPixelMap(ResourceTable.Media_def_image);
    }

    protected String obtainImageFileInfo(File imageFile, PixelMap pixelMap) {
        if ((imageFile != null) && (pixelMap != null)) {
            return String.format(Locale.ROOT,
                    "Name: %s" + "\n"
                            + "Size: %fKB" + "\n"
                            + "Width: %d" + "\n"
                            + "Height: %d",
                    imageFile.getName(),
                    (imageFile.length() / 1024f),
                    pixelMap.getImageInfo().size.width,
                    pixelMap.getImageInfo().size.height);
        } else if ((imageFile == null) && (pixelMap != null)) {
            return String.format(Locale.ROOT,
                    "Name: %s" + "\n"
                            + "Width: %d" + "\n"
                            + "Height: %d",
                    "NA",
                    pixelMap.getImageInfo().size.width,
                    pixelMap.getImageInfo().size.height);
        } else {
            return getString(ResourceTable.String_invalid_image_file);
        }
    }

    class CompressHandler extends EventHandler {
        public CompressHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            PixelMap pixelMap;
            switch (event.eventId) {
                case EVENT_COMPRESS_IMAGE_FILE:
                    File desFile = new File((String) event.object);
                    ImageSource imageSource = ImageSource.create(desFile, new ImageSource.SourceOptions());
                    pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
                    mImageOutput.setPixelMap(pixelMap);
                    mTextOutput.setText(obtainImageFileInfo(desFile, pixelMap));
                    break;
                case EVENT_COMPRESS_PIXELMAP:
                    pixelMap = (PixelMap) event.object;
                    if (pixelMap != null) {
                        mImageOutput.setPixelMap(pixelMap);
                        mTextOutput.setText(obtainImageFileInfo(null, pixelMap));
                    }
                    break;
            }
        }
    }
}

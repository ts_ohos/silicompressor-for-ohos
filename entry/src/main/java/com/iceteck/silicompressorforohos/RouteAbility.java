package com.iceteck.silicompressorforohos;

import com.iceteck.silicompressorforohos.slice.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RouteAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RouteSlice.class.getName());

        addActionRoute("action.compress.image.file", CompressImageFileSlice.class.getName());
        addActionRoute("action.compress.image.file.option", CompressImageFileWithOptionSlice.class.getName());
        addActionRoute("action.compress.image.element", CompressElementResSlice.class.getName());
        addActionRoute("action.compress.image.pixelmap", CompressToPixelMapSlice.class.getName());
        addActionRoute("action.compress.image.pixelmap.option", CompressToPixelMapWithOptionSlice.class.getName());
    }
}

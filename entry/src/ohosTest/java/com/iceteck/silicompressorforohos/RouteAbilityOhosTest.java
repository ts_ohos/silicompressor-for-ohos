package com.iceteck.silicompressorforohos;

import com.iceteck.silicompressorrforohos.SiliCompressor;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static com.iceteck.silicompressorforohos.OtherUtils.BUNDLE_SANDBOX_PREFIX;

public class RouteAbilityOhosTest {

    @Before
    public void initSourceImage() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        try {
            InputStream inputStream1 = context.getResourceManager()
                    .getResource(ResourceTable.Media_test_compress_image_pixelmap);
            InputStream inputStream2 = context.getResourceManager()
                    .getResource(ResourceTable.Media_test_compress_image_pixelmap_option);
            InputStream inputStream3 = context.getResourceManager()
                    .getResource(ResourceTable.Media_test_compress_image_file);
            InputStream inputStream4 = context.getResourceManager()
                    .getResource(ResourceTable.Media_test_compress_image_file_option);
            getFile(inputStream1, BUNDLE_SANDBOX_PREFIX + "/test_compress_image_pixelmap.jpeg");
            getFile(inputStream2, BUNDLE_SANDBOX_PREFIX + "/test_compress_image_pixelmap_option.jpeg");
            getFile(inputStream3, BUNDLE_SANDBOX_PREFIX + "/test_compress_image_file.jpeg");
            getFile(inputStream4, BUNDLE_SANDBOX_PREFIX + "/test_compress_image_file_option.jpeg");
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
    }

    /**
     * 压缩图片,并转为PixelMap
     * 断言：
     * 1.调用getCompressBitmap方法返回压缩后的PixelMap不为空；
     * 2.压缩后的文件比压缩前的文件小；
     */
    @Test
    public void compressorToPixelMap() {
        String TEST_IMAGE_FILE_PATH = BUNDLE_SANDBOX_PREFIX + "/test_compress_image_pixelmap.jpeg";

        File inputFile = new File(TEST_IMAGE_FILE_PATH);
        ImageSource imageSource = ImageSource.create(inputFile, new ImageSource.SourceOptions());
        PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        PixelMap outputPixelMap = null;
        try {
            outputPixelMap = SiliCompressor.with(context)
                    .getCompressBitmap(Uri.getUriFromFile(inputFile).toString(), BUNDLE_SANDBOX_PREFIX);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(outputPixelMap);
        long gap = outputPixelMap.getPixelBytesNumber() - pixelMap.getPixelBytesNumber();
        Assert.assertTrue(gap < 0);
    }

    /**
     * 压缩图片,转为PixelMap，并删除原文件
     * 断言：
     * 1.调用getCompressBitmap方法返回压缩后的PixelMap不为空；
     * 2.压缩后的文件比压缩前的文件小；
     * 3.原文件被删除
     */
    @Test
    public void compressorToPixelMap2Delete() {
        String TEST_IMAGE_FILE_PATH = BUNDLE_SANDBOX_PREFIX + "/test_compress_image_pixelmap_option.jpeg";

        File inputFile = new File(TEST_IMAGE_FILE_PATH);
        ImageSource imageSource = ImageSource.create(inputFile, new ImageSource.SourceOptions());
        PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        PixelMap outputPixelMap = null;
        try {
            outputPixelMap = SiliCompressor.with(context)
                    .getCompressBitmap(Uri.getUriFromFile(inputFile).toString(), true, BUNDLE_SANDBOX_PREFIX);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(outputPixelMap);
        long gap = outputPixelMap.getPixelBytesNumber() - pixelMap.getPixelBytesNumber();
        Assert.assertTrue(gap < 0);
        Assert.assertFalse(inputFile.exists());
    }

    /**
     * 压缩图片，并转为Image文件
     * 断言：
     * 1.调用compress方法返回压缩后的outputFile路径不为空；
     * 2.返回的outputFile路径图片文件存在；
     * 3.压缩后的文件比压缩前的文件小；
     */
    @Test
    public void compressorToImageFile() {
        String TEST_IMAGE_FILE_PATH = BUNDLE_SANDBOX_PREFIX + "/test_compress_image_file.jpeg";

        File inputFile = new File(TEST_IMAGE_FILE_PATH);
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

        String outputFile = SiliCompressor.with(context).
                compress(Uri.getUriFromFile(inputFile).toString(),
                        new File(BUNDLE_SANDBOX_PREFIX));
        Assert.assertNotNull(outputFile);
        File outFile = new File(outputFile);
        Assert.assertTrue(outFile.exists());
        long gap = outputFile.length() - inputFile.length();
        Assert.assertTrue(gap < 0);
    }

    /**
     * 压缩图片，转为Image文件，并删除原文件
     * 断言：
     * 1.调用compress方法返回压缩后的outputFile路径不为空；
     * 2.返回的outputFile路径图片文件存在；
     * 3.压缩后的文件比压缩前的文件小；
     * 4.原文件被删除
     */
    @Test
    public void compressorToImageFile2Delete() {
        String TEST_IMAGE_FILE_PATH = BUNDLE_SANDBOX_PREFIX + "/test_compress_image_file_option.jpeg";

        File inputFile = new File(TEST_IMAGE_FILE_PATH);
        final long sourceLength = inputFile.length();

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

        String outputFile = SiliCompressor.with(context).
                compress(Uri.getUriFromFile(inputFile).toString(),
                        new File(BUNDLE_SANDBOX_PREFIX), true);
        Assert.assertNotNull(outputFile);
        File outFile = new File(outputFile);
        Assert.assertTrue(outFile.exists());
        long gap = outputFile.length() - sourceLength;
        Assert.assertTrue(gap < 0);
        Assert.assertFalse(inputFile.exists());
    }

    /**
     * 压缩element
     * 断言：
     * 1.调用compress方法返回压缩后的outputFile路径不为空；
     * 2.返回的outputFile路径图片文件存在；
     * 3.压缩后的文件比压缩前的文件小；
     */
    @Test
    public void compressorElement() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String outputFile = SiliCompressor.with(context)
                .compress(ResourceTable.Media_test_compress_element, BUNDLE_SANDBOX_PREFIX);
        Assert.assertNotNull(outputFile);
        File outFile = new File(outputFile);
        Assert.assertTrue(outFile.exists());
        PixelMap bitmap = getPixelMapFromResource(context, ResourceTable.Media_test_compress_element);
        long gap = outFile.length() - bitmap.getPixelBytesNumber();
        Assert.assertTrue(gap < 0);
    }

    private PixelMap getPixelMapFromResource(Context context, int resourceId) {
        try (InputStream inputStream = context.getResourceManager().getResource(resourceId)) {
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/jpg";
            ImageSource imageSource = ImageSource.create(inputStream, srcOpts);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            // do Nothing here
        }
        return null;
    }

    public static void getFile(InputStream is, String fileName) throws IOException {
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        in = new BufferedInputStream(is);
        out = new BufferedOutputStream(new FileOutputStream(fileName));
        int len = -1;
        byte[] b = new byte[1024];
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
        in.close();
        out.close();
    }
}